import os
from flask import Flask, request, jsonify, make_response, abort
from flask import url_for, redirect, render_template
from werkzeug.utils import secure_filename
from celery import Celery

app = Flask(__name__)

images = [
    {
        'id': 1,
        'image': '',
        'style': '',
        'status': 'WAIT'
    }
]



@app.route('/', methods=['GET'])
def show_landing_page():
    return render_template('start.html')


@app.route('/api/v1.0/image/status')
def hello_world():
    return 'Hello World!'
UPLOAD_FOLDER_IMG = '/Users/denissaklein1/PycharmProjects/art_server_flask/images'
UPLOAD_FOLDER_STYLE = '/Users/denissaklein1/PycharmProjects/art_server_flask/styles'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER_IMG'] = UPLOAD_FOLDER_IMG
app.config['UPLOAD_FOLDER_STYLE'] = UPLOAD_FOLDER_STYLE


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def set_filename(filename):
    expansion = secure_filename(filename).rsplit('.', 1)[1]
    return str(images[-1]['id'] + 1) + '.' + expansion

@app.route('/api/v1.0/image/upload_page')
def show_success_upload():
    return 'Please, wait for result'

@app.route('/api/v1.0/image/new/', methods=['POST'])
def get_new_image():
    print(request.files['file_style'], request.files)
    #if not request.json or 'title' not in request.json:
    #  abort(400)
    file_img = request.files['file_img']
    file_style = request.files['file_style']
    print(file_style)
    print(file_img)
    if (file_img and allowed_file(file_img.filename)) \
            and (file_style and allowed_file(file_style.filename)):

        filename_img = 'img_' + set_filename(file_img.filename)
        print(os.path.join(app.config['UPLOAD_FOLDER_IMG'],
                                   filename_img))

        file_img.save(os.path.join(app.config['UPLOAD_FOLDER_IMG'],
                                   filename_img))
        filename_style = 'style_' +set_filename(file_img.filename)
        print(file_style)
        file_style.save(os.path.join(app.config['UPLOAD_FOLDER_STYLE'],
                                     filename_style))
        images.append(
            {
                'id': images[-1]['id'] + 1,
                'image': filename_img,
                'style': file_style,
                'status': 'PREPARE'
            }
        )
        return show_success_upload()
    return show_landing_page()


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/longtask', methods=['POST'])
def longtask():
    task = long_task.apply_async()
    return jsonify({}), 202, {'Location': url_for('taskstatus',
                                                  task_id=task.id)}


if __name__ == '__main__':
    app.debug = True
    app.run()
