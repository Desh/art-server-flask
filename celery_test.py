from flask import Flask, render_template, jsonify, url_for, redirect
from celery import Celery
from subprocess import Popen, PIPE
import time, sys, shlex
app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
app.config['CELERY_MAX_TASKS_PER_CHILD'] = 1

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)
localhost = 'http://127.0.0.1:5000'


@celery.task(bind=True)
def long_task(self, args):
    sys.stdout.write('1')
    proc = Popen(shlex.split('python script.py --times {}'.format(args[0])), stdout=PIPE, stderr=PIPE, shell=False)
    self.update_state(state='BEGIN', meta={'iterations': 0, 'hole': args[0]})
    print('begin')
    while True:
        out = proc.stdout.readline().decode()
        sys.stdout.write(out)
        sys.stdout.flush()
        if out == '' and proc.poll() is not None:
            break
        print(out)
        if out != '':
            self.update_state(state='PROGRESS', meta={'iteration': out, 'hole': args[0]})
    proc.wait()
    return {'status': 'Task completed!', 'hole': 1, 'iteration': 50}


@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = long_task.AsyncResult(task_id)
    response = {
            'state': task.state,
            'iteration': task.info.get('iteration'),
            'hole':  task.info.get('hole')
    }
    return jsonify(response)


@app.route('/', methods=['GET'])
def start_page():
    return render_template('landing.html')

@app.route('/celery_task', methods=['GET'])
def start_task():
    task = long_task.apply_async(['10'])
    #return jsonify({'smth': task.id, 'Location': '127.0.0.1:5000' + url_for('taskstatus',
    #              task_id=task.id)}), 202
    return redirect('{}/status/{}'.format(localhost, task.id), code=202)
if __name__ == '__main__':
    app.debug = True
    app.run()