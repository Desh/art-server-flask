import time
from argparse import ArgumentParser
import sys
def build_parser():
    parser = ArgumentParser()
    parser.add_argument('--times',
            dest='times', required=True)
    return parser

def main():
    parser = build_parser()
    options = parser.parse_args()
    for i in range(10):
        print i
        time.sleep(1)


if __name__ == '__main__':
    main()
