from celery import current_app
from celery.bin import worker


if __name__ == '__main__':
    app = current_app._get_current_object()

    worker = worker.worker(app=app)

    options = {
        'loglevel': 'INFO',
        'traceback': True,
    }

    worker.run(**options)