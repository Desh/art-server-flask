from subprocess import Popen, PIPE
from queue import Queue
import shlex, sys, time


args = '500'
proc = Popen('python script.py --times {}'.format(args), stdout=PIPE, stderr=PIPE, shell=True)
while True:
    out = proc.stdout.read(1).decode()
    if out == '' and proc.poll() is not None:
        break
    if out != '':
    	sys.stdout.write(out)
    	sys.stdout.flush()
proc.wait()
res = proc.communicate()
if proc.returncode:
    print(res[1].decode())
print(res[0].decode())